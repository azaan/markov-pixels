import random
import math
from PIL import Image

im = Image.open('pixel.png')
pix = im.load()

im2 = Image.new(im.mode, im.size)

rawColors = [(42, 68, 123), (104, 178, 205),
(254, 244, 191), (18, 35, 46), (49, 20, 35), (247, 191, 52),
(11, 86, 149)]


imData = []
for i in range(50 * 50):
    idx = math.floor(random.random() * len(rawColors))

    imData.append(rawColors[idx])

im2.putdata(imData)
im2.save('random.png')
