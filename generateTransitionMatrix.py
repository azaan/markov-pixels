from PIL import Image
import json

im = Image.open('new.png')
pix = im.load()

rawColors = [(42, 68, 123), (104, 178, 205),
(254, 244, 191), (18, 35, 46), (49, 20, 35), (247, 191, 52),
(11, 86, 149)]

colorDict = dict()
for i, color in enumerate(rawColors):
    colorDict[color] = i

# serialize image into array of pixels
serialized = []
for y in range(im.size[0]):
    for x in range(im.size[1]):
        serialized.append(pix[x, y])


# generate a matrix
matrix = []
for i in range(len(rawColors)):
    r = [0 for x in range(len(rawColors))]
    matrix.append(r)

for i in range(1, len(serialized)):
    current = serialized[i]
    prev = serialized[i-1]

    cCol = colorDict[(current[0], current[1], current[2])]
    pCol = colorDict[(prev[0], prev[1], prev[2])]

    matrix[pCol][cCol] += 1

# normalize
newMatrix = []
for row in matrix:
    total = sum(row)

    t = []
    for item in row:
        t.append(item / total)

    newMatrix.append(t)

print(json.dumps(newMatrix))
