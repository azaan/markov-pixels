from PIL import Image
import operator

from colormath.color_objects import sRGBColor, LabColor
from colormath.color_conversions import convert_color
from colormath.color_diff import delta_e_cie2000

im = Image.open('pixel.png')
pix = im.load()

im2 = Image.new(im.mode, im.size)

c1 = convert_color(sRGBColor(42, 68, 123), LabColor)
c2 = convert_color(sRGBColor(104, 178, 205), LabColor)
c3 = convert_color(sRGBColor(254, 244, 191), LabColor)
c4 = convert_color(sRGBColor(18, 35, 46), LabColor)
c5 = convert_color(sRGBColor(49, 20, 35), LabColor)
c6 = convert_color(sRGBColor(247, 191, 52), LabColor)
c7 = convert_color(sRGBColor(11, 86, 149), LabColor)

rawColors = [(42, 68, 123), (104, 178, 205),
(254, 244, 191), (18, 35, 46), (49, 20, 35), (247, 191, 52),
(11, 86, 149)]
cs = [c1, c2, c3, c4, c5, c6, c7]

def closest_color(o):
    col = convert_color(sRGBColor(o[0], o[1], o[2]), LabColor)

    smallest = 99999999
    di = None
    for i, c in enumerate(cs):
        delta = delta_e_cie2000(c, col)

        if delta < smallest:
            smallest = delta
            di = i

    return rawColors[di]

imData = []
for y in range(im.size[0]):
    for x in range(im.size[1]):
        r, g, b, a = pix[x, y]
        o = (r, g, b)

        # normalize r g b values
        o = closest_color(o)

        imData.append(o)

im2.putdata(imData)
im2.save('new.png')
